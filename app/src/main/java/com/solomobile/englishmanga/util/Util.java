package com.solomobile.englishmanga.util;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;

/**
 * Created by igorschkrab on 2015-08-02.
 */
public class Util {
    public static void startMyTask(AsyncTask<Context, Void, Void> asyncTask, Context... params) {
        try{
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
            else
                asyncTask.execute(params);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
