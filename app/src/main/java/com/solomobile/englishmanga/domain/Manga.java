package com.solomobile.englishmanga.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Manga implements Serializable{
	private static final long serialVersionUID = 1L;
	private String name;
	private String link;
	private List<Chapter> chapterList = new ArrayList<Chapter>();
	private String coverLink;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public List<Chapter> getChapterList() {
		return chapterList;
	}

	public void setChapterList(List<Chapter> chapterList) {
		this.chapterList = chapterList;
	}

	public String getCoverLink() {
		return coverLink;
	}

	public void setCoverLink(String coverLink) {
		this.coverLink = coverLink;
	}
}
