package com.solomobile.englishmanga.controller;

import com.solomobile.englishmanga.domain.Chapter;
import com.solomobile.englishmanga.domain.Manga;
import com.solomobile.englishmanga.web.MangaFoxScrapping;

import java.util.List;

/**
 * Created by igorschkrab on 2015-08-02.
 */
public class EnglishMangaController {

    private static EnglishMangaController controller = null;

    private EnglishMangaController(){

    }

    public static EnglishMangaController getInstance(){
        if(controller == null){
            controller = new EnglishMangaController();
        }
        return controller;
    }

    public List<Manga> getPopularMangas(){
        return MangaFoxScrapping.getMostPopularMangas();
    }

    public List<Manga> getRepositoryMangas(){
        return MangaFoxScrapping.getMangaList();
    }

    public void defineMangaCover(List<Manga> mangaList) {
        MangaFoxScrapping.getMangaCover(mangaList);
    }

    public List<Chapter> getMangaChapters(Manga manga){
        return MangaFoxScrapping.getMangaEpisodes(manga);
    }

    public void defineMangaCover(Manga manga) {
        MangaFoxScrapping.getMangaCoverForManga(manga);
    }
}
