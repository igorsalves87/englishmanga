package com.solomobile.englishmanga.task;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.GridView;
import android.widget.AbsListView;
import com.solomobile.englishmanga.R;
import com.solomobile.englishmanga.adapter.MangaGridArrayAdapter;
import com.solomobile.englishmanga.controller.EnglishMangaController;
import com.solomobile.englishmanga.domain.Manga;
import com.solomobile.englishmanga.util.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by igorschkrab on 2015-08-02.
 */
public class PopularMangaTask extends AsyncTask<Context, Void, Void> {

    private Activity activity = null;
    private MangaGridArrayAdapter mangaGridArrayAdapter = null;
    private List<Manga> mangas = null;
    String erro = null;
    private final int MAX_PER_PAGE = 6;
    private static int currentPage = 0;
    private static Boolean loading = new Boolean(false);



    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    @Override
    protected synchronized Void doInBackground(Context... params) {
        this.activity = (Activity) params[0];
        if(mangas == null) {
            System.out.println("Getting Mangas first time");
            EnglishMangaController controller = EnglishMangaController.getInstance();
            mangas = controller.getPopularMangas();
            List<Manga> mangaList = mangas.subList(currentPage * MAX_PER_PAGE, (currentPage * MAX_PER_PAGE) + MAX_PER_PAGE);
            List<Manga> gridList = new ArrayList<Manga>();
            gridList.addAll(mangaList);
            mangaGridArrayAdapter = new MangaGridArrayAdapter(activity, gridList);
        }
        System.out.println("Previous Current Page: " + currentPage);
        currentPage++;
        System.out.println("After Current Page: " + currentPage);
        return null;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public List<Manga> getMangas() {
        return mangas;
    }

    public void setMangas(List<Manga> mangas) {
        this.mangas = mangas;
    }

    public MangaGridArrayAdapter getMangaGridArrayAdapter() {
        return mangaGridArrayAdapter;
    }

    public void setMangaGridArrayAdapter(MangaGridArrayAdapter mangaGridArrayAdapter) {
        this.mangaGridArrayAdapter = mangaGridArrayAdapter;
    }

    @Override
    protected synchronized void onPostExecute(Void result) {
        super.onPostExecute(result);
        try{

            GridView gridView = (GridView) activity.findViewById(R.id.gridviewPopular);
            if(gridView != null){
                if(currentPage == 1) {
                    gridView.setAdapter(mangaGridArrayAdapter);
                    gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
                        @Override
                        public void onScrollStateChanged(AbsListView view, int scrollState) {

                        }

                        @Override
                        public synchronized void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                            int lastInScreen = firstVisibleItem + visibleItemCount;
                            int maxPages = (int)(mangas.size() / MAX_PER_PAGE);
                            System.out.println("Scrolling - CurrentPage: " + currentPage + " Max Pages: " + maxPages + " Loading: " + loading);
                            if (!loading && lastInScreen == totalItemCount && currentPage <= maxPages) {
                                loading = true;
                                System.out.println("Call new popular manga task: " + loading);
                                    PopularMangaTask popularMangaTask = new PopularMangaTask();
                                    popularMangaTask.setCurrentPage(currentPage);
                                    popularMangaTask.setMangas(mangas);
                                    popularMangaTask.setMangaGridArrayAdapter(mangaGridArrayAdapter);
                                    Util.startMyTask(popularMangaTask, activity);
                            }
                        }
                    });
                }else{
                    System.out.println("Adding More Mangas to the Popular list - Current Page: " + currentPage);
                    int maxSubList = ((currentPage-1) * MAX_PER_PAGE) + MAX_PER_PAGE;
                    System.out.println("MaxSubList: " + maxSubList);
                    System.out.println("Mangas.size: " + mangas.size());
                    List<Manga> subListManga = new ArrayList<Manga>();
                    int index = maxSubList >= mangas.size() ? mangas.size() : maxSubList;
                    System.out.println("i = " + (currentPage-1) * MAX_PER_PAGE);
                    System.out.println("Condition: " + index);
                    for(int i = (currentPage-1) * MAX_PER_PAGE; i < index; i++){
                        subListManga.add(mangas.get(i));
                    }
                    System.out.println("subListManga.size: " + subListManga.size());
                    mangaGridArrayAdapter.addAll(subListManga);
                    loading = false;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
