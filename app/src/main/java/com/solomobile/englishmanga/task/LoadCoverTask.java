package com.solomobile.englishmanga.task;

import android.app.Activity;
import android.content.Context;
import android.media.Image;
import android.os.AsyncTask;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.ImageView;

import com.solomobile.englishmanga.R;
import com.solomobile.englishmanga.adapter.MangaGridArrayAdapter;
import com.solomobile.englishmanga.controller.EnglishMangaController;
import com.solomobile.englishmanga.domain.Manga;
import com.solomobile.englishmanga.util.Util;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by igorschkrab on 2015-08-02.
 */
public class LoadCoverTask extends AsyncTask<Context, Void, Void> {

    private Activity activity = null;
    private Context context = null;
    private Manga manga = null;
    private ImageView imageView = null;


    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    @Override
    protected synchronized Void doInBackground(Context... params) {
        this.activity = (Activity) params[0];
        EnglishMangaController.getInstance().defineMangaCover(manga);
        return null;
    }

    @Override
    protected synchronized void onPostExecute(Void result) {
        super.onPostExecute(result);
        System.out.println("Loading image Task: " + manga.getCoverLink());
        Picasso.with(context).load(manga.getCoverLink()).into(imageView);
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public Manga getManga() {
        return manga;
    }

    public void setManga(Manga manga) {
        this.manga = manga;
    }

    public ImageView getImageView() {
        return imageView;
    }

    public void setImageView(ImageView imageView) {
        this.imageView = imageView;
    }
}
