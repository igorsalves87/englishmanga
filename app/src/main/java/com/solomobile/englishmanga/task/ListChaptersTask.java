package com.solomobile.englishmanga.task;

import java.util.Collections;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.AbsListView;
import android.widget.ProgressBar;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;

import com.solomobile.englishmanga.R;
import com.solomobile.englishmanga.adapter.ChapterArrayAdapter;
import com.solomobile.englishmanga.controller.EnglishMangaController;
import com.solomobile.englishmanga.domain.Chapter;
import com.solomobile.englishmanga.domain.Manga;

public class ListChaptersTask extends AsyncTask<Context, Void, Void> {
	private Activity activity = null;
	private ChapterArrayAdapter chapterArrayAdapter = null;
	private Manga manga = null;
	private List<Chapter> chapters = null;
	private static boolean carregando = false;

	@Override
	protected Void doInBackground(Context... params) {
		Log.i("doInBrackground", "Inicio");
		this.activity = (Activity) params[0];
		manga = (Manga) activity.getIntent().getSerializableExtra("manga");
		EnglishMangaController controller = EnglishMangaController.getInstance();
		chapters = controller.getMangaChapters(manga);

		Log.i("doInBrackground", "Pagina Inicial");
		chapterArrayAdapter = new ChapterArrayAdapter(activity, chapters);
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		try{
			super.onPostExecute(result);
			Log.i("onPostExecute", "Pagina Inicial");
			final ListView listView = (ListView) activity.findViewById(R.id.chaptersListView);
			if(listView != null && chapterArrayAdapter != null){
				listView.setAdapter(chapterArrayAdapter);
			}
		}catch (Exception e){
			e.printStackTrace();
		}
	}

}
