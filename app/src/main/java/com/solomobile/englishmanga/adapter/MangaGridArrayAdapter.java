package com.solomobile.englishmanga.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.solomobile.englishmanga.R;
import com.solomobile.englishmanga.controller.EnglishMangaController;
import com.solomobile.englishmanga.domain.Manga;
import com.solomobile.englishmanga.task.LoadCoverTask;
import com.solomobile.englishmanga.util.Util;
import com.squareup.picasso.Picasso;

public class MangaGridArrayAdapter extends ArrayAdapter<Manga>{
	private final Context context;
	private final List<Manga> mangas;

	public List<Manga> getMangas() {
		return mangas;
	}

	public MangaGridArrayAdapter(Context context, List<Manga> mangas) {
		super(context, R.layout.manga_grid_layout, mangas);
		this.context = context;
		this.mangas = mangas;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final View rowView = inflater.inflate(R.layout.manga_grid_layout, parent, false);
		final Manga selectedManga = mangas.get(position);
		final ImageView imageView = (ImageView) rowView.findViewById(R.id.iconManga);


		if(selectedManga.getCoverLink() == null) {
			LoadCoverTask loadCoverTask = new LoadCoverTask();
			loadCoverTask.setContext(context);
			loadCoverTask.setImageView(imageView);
			loadCoverTask.setManga(selectedManga);
			Util.startMyTask(loadCoverTask, context);
		}else {
			System.out.println("Loading image getView: " + selectedManga.getCoverLink());
			Picasso.with(context).load(selectedManga.getCoverLink()).into(imageView);
		}

		TextView textView = (TextView) rowView.findViewById(R.id.name);
		textView.setText(selectedManga.getName());
		return rowView;
	}
}
