package com.solomobile.englishmanga.adapter;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.solomobile.englishmanga.R;
import com.solomobile.englishmanga.domain.Chapter;

public class ChapterArrayAdapter extends ArrayAdapter<Chapter> {
	private final Context context;
	private final List<Chapter> chapters;

	public ChapterArrayAdapter(Context context, List<Chapter> chapters) {
		super(context, R.layout.chapter_layout, chapters);
		this.context = context;
		this.chapters = chapters;
	}
	

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final View rowView = inflater.inflate(R.layout.chapter_layout, parent, false);

		Chapter chapter = chapters.get(position);

		TextView textView = (TextView) rowView.findViewById(R.id.chapter);
		textView.setText(chapter.getName());
//		if (chapter.isExiste()) {
//			ImageView imageView = (ImageView) rowView.findViewById(R.id.iconDownload);
//			imageView.setVisibility(View.GONE);
//		} else if(Controller.getInstance().isBaixandoCapitulo(chapter)){
//			ImageView imageView = (ImageView) rowView.findViewById(R.id.iconDownload);
//			imageView.setColorFilter(Color.parseColor(context.getString(R.string.cor)));
//			imageView.setContentDescription(chapter.getNumero());
//			imageView.setImageResource(android.R.drawable.stat_sys_download);
//		}else{
		configuraImagemDownload(rowView, chapter);
//		}
		return rowView;
	}

	private void configuraImagemDownload(final View rowView, Chapter capituloSelecionado) {
		ImageView imageView = (ImageView) rowView.findViewById(R.id.iconDownload);
		imageView.setColorFilter(Color.parseColor(context.getString(R.string.color)));
		imageView.setContentDescription(capituloSelecionado.getName());
//		imageView.setOnClickListener(onDownloadClickListener(capituloSelecionado));
	}

//	private OnClickListener onDownloadClickListener(final Chapter capituloSelecionado) {
//		return new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				Controller.getInstance().adicionaCapituloListaDownloads(capituloSelecionado);
//				Intent intent = new Intent(context, DownloadService.class);
//				intent.putExtra("capitulo", capituloSelecionado);
//			    context.startService(intent);
//			    ImageView imageView = (ImageView) v;
//			    imageView.setImageResource(android.R.drawable.stat_sys_download);
//			    capituloSelecionado.setExiste(true);
//				v.setOnClickListener(null);
//			}
//		};
//	}
	
	
}
