package com.solomobile.englishmanga.web;

import com.solomobile.englishmanga.domain.Manga;
import com.solomobile.englishmanga.domain.Chapter;
import com.solomobile.englishmanga.util.JSoapUtil;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by igorschkrab on 2015-08-02.
 */
public class MangaFoxScrapping {

    public static List<Manga> getMostPopularMangas(){
        System.out.println("Begin - getMostPopularMangas - " + new Date());
        List<Manga> popularMangas = new ArrayList<Manga>();
        try {
            Document mangaFoxHTML = JSoapUtil.getDocument("http://mangafox.me/");
            popularMangas = getNameAndLinkPopularMangas(mangaFoxHTML);
//            getMangaCover(popularMangas);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("End - getMostPopularMangas - " + new Date());
        return popularMangas;
    }


    public static List<Manga> getMangaList() {
        System.out.println("Begin - getMangaList - " + new Date());
        List<Manga> mangaList = new ArrayList<Manga>();
        try {
            Document mangaFoxHTML = JSoapUtil.getDocument("http://mangafox.me/manga/");
            Elements mangaListDiv = mangaFoxHTML.select("div[class=manga_list]").select("a");
            for (Iterator<Element> iteratorLinksAndNames = mangaListDiv.iterator(); iteratorLinksAndNames.hasNext(); ) {
                Element link = (Element) iteratorLinksAndNames.next();
                try {
                    Manga manga = new Manga();
                    manga.setName(link.html());
                    manga.setLink(link.absUrl("href"));
                    mangaList.add(manga);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }catch (IOException e){
            e.printStackTrace();
            //TODO - Show some error message
        }
        System.out.println("MangaList.size: " + mangaList.size());
        System.out.println("End - getMangaList - " + new Date());
        if(mangaList.size() > 0) {
            mangaList.remove(0);
        }
        return mangaList;
    }

    private static List<Manga> getNameAndLinkPopularMangas(Document document){
        System.out.println("Begin - getNameAndLinkPopularMangas - " + new Date());
        List<Manga> mangaList = new ArrayList<Manga>();
        Elements popularDiv = document.select("div[id=popular]");
        Elements mangaLinksAndNames = popularDiv.select("div[class=nowrap]").select("a[class=series_preview top]");
        for (Iterator<Element> iteratorLinksAndNames = mangaLinksAndNames.iterator(); iteratorLinksAndNames.hasNext();) {
            Element link = (Element) iteratorLinksAndNames.next();
            try {
                Manga manga = new Manga();
                manga.setName(link.html());
                manga.setLink(link.absUrl("href"));
                mangaList.add(manga);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        System.out.println("End - getNameAndLinkPopularMangas - " + new Date());
        return mangaList;
    }

    public static void getMangaCover(List<Manga> mangaList) {
        System.out.println("Begin - getMangaCover - " + new Date());
        System.out.println("Manga Size: " + mangaList.size());
        for(Manga manga : mangaList){
            try {
                Document mangaPageHTML = JSoapUtil.getDocument(manga.getLink());
                Elements coverImg = mangaPageHTML.select("div[class=cover]").select("img");
                manga.setCoverLink(coverImg.attr("src"));
                System.out.println("Setting cover: " + manga.getCoverLink());
            }catch (IOException e){
                e.printStackTrace();
                //TODO - set default image
            }
        }
        System.out.println("End - getMangaCover - " + new Date());
    }

    public static void getMangaCoverForManga(Manga manga) {
        System.out.println("End - getMangaCoverForManga - " + new Date());
        try {
            Document mangaPageHTML = JSoapUtil.getDocument(manga.getLink());
            Elements coverImg = mangaPageHTML.select("div[class=cover]").select("img");
            manga.setCoverLink(coverImg.attr("src"));
            System.out.println("Got Manga Cover: " + manga.getCoverLink());
        }catch (IOException e){
            e.printStackTrace();
            //TODO - set default image
        }
        System.out.println("End - getMangaCoverForManga - " + new Date());
    }

    public static List<Chapter> getMangaEpisodes(Manga manga) {
        List<Chapter> chapters = new ArrayList<Chapter>();
        try {
            Document mangaHTML = JSoapUtil.getDocument(manga.getLink());
            Elements divChapters = mangaHTML.select("div[id=chapters]");
            Elements mangaChapterLinks = divChapters.select("a[class=tips]");

            for (Iterator<Element> iteratorMangaChapters = mangaChapterLinks.iterator(); iteratorMangaChapters.hasNext(); ) {

                Element link = (Element) iteratorMangaChapters.next();
                try {
                    Chapter chapter = new Chapter();
                    chapter.setName(link.html());
                    chapter.setLink(link.absUrl("href"));
                    chapter.setManga(manga);
                    chapters.add(chapter);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }catch (IOException e){
            //TODO - return no chapters
        }
        Collections.reverse(chapters);
        return chapters;
    }

    public static List<Chapter> getMangaEpisodes(List<Manga> mangaList) throws IOException {
        List<Chapter> chapters = new ArrayList<Chapter>();
        for(Manga manga : mangaList) {
            Document mangaHTML = JSoapUtil.getDocument(manga.getLink());
            Elements divChapters = mangaHTML.select("div[id=chapters]");
            Elements mangaChapterLinks = divChapters.select("a[class=tips]");

            for (Iterator<Element> iteratorMangaChapters = mangaChapterLinks.iterator(); iteratorMangaChapters.hasNext(); ) {

                Element link = (Element) iteratorMangaChapters.next();
                try {
                    Chapter chapter = new Chapter();
                    chapter.setName(link.html());
                    chapter.setLink(link.absUrl("href"));
                    chapter.setManga(manga);
                    chapters.add(chapter);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return chapters;
    }



}
