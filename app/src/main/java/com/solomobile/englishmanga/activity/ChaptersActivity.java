package com.solomobile.englishmanga.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.solomobile.englishmanga.R;
import com.solomobile.englishmanga.controller.EnglishMangaController;
import com.solomobile.englishmanga.domain.Chapter;
import com.solomobile.englishmanga.domain.Manga;
import com.solomobile.englishmanga.task.ListChaptersTask;
import com.solomobile.englishmanga.util.Util;


public class ChaptersActivity extends Activity {
	private Manga manga;
	private ListView listView;
	private Activity activity;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.chapters_activity);
		manga = (Manga) getIntent().getSerializableExtra("manga");
		setTitle(Html.fromHtml("<font color='#FFFFFF'>" + manga.getName() + "</font>"));

		listView = (ListView) findViewById(R.id.chaptersListView);
		activity = this;

		ListChaptersTask listChaptersTask = new ListChaptersTask();

		Util.startMyTask(listChaptersTask, this);
	}

}
