package com.solomobile.englishmanga.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;

import com.solomobile.englishmanga.MainActivity;
import com.solomobile.englishmanga.R;
import com.solomobile.englishmanga.domain.Manga;
import com.solomobile.englishmanga.task.LoadMangaEpisodesTask;
import com.solomobile.englishmanga.task.RepositoryMangaTask;
import com.solomobile.englishmanga.util.Util;

/**
     * A placeholder fragment containing a simple view.
     */
    public class MangaEpisodesFragment extends Fragment {
        private ListView listView;
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
//        public static MangaEpisodesFragment newInstance(int sectionNumber) {
//            MangaEpisodesFragment fragment = new MangaEpisodesFragment();
//            Bundle args = new Bundle();
//            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
//            fragment.setArguments(args);
//            return fragment;
//        }

        public MangaEpisodesFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.manga_episodes_fragment, container, false);

//            setContentView(R.layout.manga_offline_activity);
//            Manga manga = (Manga) getIntent().getSerializableExtra("manga");
//            setTitle("");

            listView = (ListView) view.findViewById(R.id.listview);

//            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                @Override
//                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                    Capitulo capitulo = (Capitulo) parent.getItemAtPosition(position);
//                    Intent i = new Intent(parent.getContext(), CapituloOfflineActivity.class);
//                    i.putExtra("capitulo", capitulo);
//                    startActivity(i);
//                }
//            });
            final LoadMangaEpisodesTask loadMangaEpisodesTask = new LoadMangaEpisodesTask();
//            loadMangaEpisodesTask.setListView(listView);
            Util.startMyTask(loadMangaEpisodesTask);

            return view;
        }

//        @Override
//        public void onAttach(Activity activity) {
//            super.onAttach(activity);
//            ((MainActivity) activity).onSectionAttached(getArguments().getInt(ARG_SECTION_NUMBER));
//        }
    }
