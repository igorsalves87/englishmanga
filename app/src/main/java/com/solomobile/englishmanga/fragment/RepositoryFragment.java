package com.solomobile.englishmanga.fragment;

import android.app.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.solomobile.englishmanga.MainActivity;
import com.solomobile.englishmanga.R;
import com.solomobile.englishmanga.domain.Manga;
import com.solomobile.englishmanga.task.PopularMangaTask;
import com.solomobile.englishmanga.task.RepositoryMangaTask;
import com.solomobile.englishmanga.util.Util;

/**
     * A placeholder fragment containing a simple view.
     */
    public class RepositoryFragment extends Fragment {
        private GridView listView;
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static RepositoryFragment newInstance(int sectionNumber) {
            RepositoryFragment fragment = new RepositoryFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public RepositoryFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.popular_fragment, container, false);

            listView = (GridView) view.findViewById(R.id.gridviewPopular);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                    Manga manga = (Manga) parent.getItemAtPosition(position);
//                    Intent i = new Intent(parent.getContext(), MainActivity.class);
//                    i.putExtra("manga", manga);
//                    startActivity(i);

                    MangaEpisodesFragment mangaEpisodesFragment = new MangaEpisodesFragment();
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(android.R.id.content, mangaEpisodesFragment);
                    fragmentTransaction.commit();
                }
            });

            RepositoryMangaTask repositoryMangaTask = new RepositoryMangaTask();
            repositoryMangaTask.setCurrentPage(0);
            repositoryMangaTask.setActivity(getActivity());
            Util.startMyTask(repositoryMangaTask, view.getContext());

            return view;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((MainActivity) activity).onSectionAttached(getArguments().getInt(ARG_SECTION_NUMBER));
        }
    }
