package com.solomobile.englishmanga.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.solomobile.englishmanga.MainActivity;
import com.solomobile.englishmanga.R;
import com.solomobile.englishmanga.activity.ChaptersActivity;
import com.solomobile.englishmanga.domain.Manga;
import com.solomobile.englishmanga.task.PopularMangaTask;
import com.solomobile.englishmanga.util.Util;

/**
     * A placeholder fragment containing a simple view.
     */
    public class PopularFragment extends Fragment {

        private GridView listView;
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PopularFragment newInstance(int sectionNumber) {
            PopularFragment fragment = new PopularFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PopularFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.popular_fragment, container, false);

            listView = (GridView) view.findViewById(R.id.gridviewPopular);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Manga manga = (Manga) parent.getItemAtPosition(position);
                    Intent i = new Intent(parent.getContext(), ChaptersActivity.class);
                    i.putExtra("manga", manga);
                    startActivity(i);
                }
            });

            PopularMangaTask popularMangaTask = new PopularMangaTask();
            popularMangaTask.setCurrentPage(0);
            popularMangaTask.setActivity(getActivity());
            Util.startMyTask(popularMangaTask, view.getContext());

            return view;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((MainActivity) activity).onSectionAttached(getArguments().getInt(ARG_SECTION_NUMBER));
        }
    }
